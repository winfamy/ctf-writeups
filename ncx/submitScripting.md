```javascript
let author = '(\_/)'
while (!ncxPwned) {
	// todo
}
```



# Cyber Combat 🔫 🖥 			        Intel Scripting

### Intro

This will cover how the submit scripts were written for both the MySQL and DNS services, and how to employ the same methods in order to automate other common web tasks.

##### Concepts to know (and links to learn)

* [Python (arrays, dicts)](https://medium.com/the-renaissance-developer/python-101-data-structures-a397bcc2bd30)
* [curl](https://curl.haxx.se/docs/manpage.html)

##### Script sources

* [MySQL](./resources/getFlagsFromMysql.py)
* [DNS Zone Transfer](./resources/getFlagsFromDNS.py)



### Pulling flags

This is obviously very service dependent, but the method of isolating a flag string is similar across multiple services.

##### DNS

Coming directly from `dig axfr @<nameserver> <domain> `

```
; <<>> DiG 9.10.6 <<>> axfr @<nameserver> <domain>
; (1 server found)
;; global options: +cmd
intelString 300	IN	TXT	"intelString.intel.teamXX.ncx2019.com"
```

So we know where the flag is from the *command line*, but how do we implement this and interact with it in Python? That's where everyone's favorite CTF module **[os](https://docs.python.org/3/library/os.html)** comes in. **os** can be used to interact directly with the operating system, namely running commands and reading output from stdin/stdout/stderr. 

It's as easy as copying and pasting this code and changing the command:

`text = os.popen(command).read()`

So for interacting with our dig command, we use

```python
text = os.popen('dig axfr @<nameserver> <domain>').read()

# Output
# ; <<>> DiG 9.10.6 <<>> axfr @<nameserver> <domain>
# ; (1 server found)
# ;; global options: +cmd
# intelString 300	IN	TXT	"intelString.intel.teamXX.ncx2019.com"
```

The rest is simply string manipulation

```python
# Splits newline characters, so separates each line into a separate index in an array
print(text.split('\n'))
# ['', '; <<>> DiG 9.10.6 <<>> axfr @<nameserver> <domain>', '; (1 server found)', ';; global options: +cmd', 'intelString 300\tIN\tTXT\t"intelString.intel.teamXX.ncx2019.com"', '']

# Takes the 2nd index from the END of the array, indicated by the -2
print(text.split('\n')[-2])
# 'intelString 300	IN	TXT	"intelString.intel.teamXX.ncx2019.com"'

# Splits spaces, same idea as splitting on \n, then takes the first index
print(text.split('\n')[-2].split(' ')[0])
# 'intelString'
```

So now we can store it in `flag` and use it for our `submit` call (covered later)

##### MySQL

Now that we've covered the basics of interacting with command output, and then isolating the flag string through DNS, we can use the same ideas against MySQL servers

Our command from `mysql -u <username> -h <server host> -p<password> <databasename> -e 'query to execute'` when used with the CyberCombat exploit of logging in with a user with a crackable password looks like this (full: `mysql -u grady -h 192.168.1.1 -pSECUREPASSWORD intel -e 'select * from intel'`)

```
invalidIntelString
invalidIntelString
...
invalidIntelString
validIntelString
```

Using what we learned earlier, it's again just copying and pasting then modifying the strings stuff to get what we want.

```python
text = os.popen("mysql -u grady -h 192.168.1.1 -pSECUREPASSWORD intel -e 'select * from intel'").read()
flag = text.split('\n')[-2]
print(flag)
# 'validIntelString'
```



### Submitting Flags

Yay, now that we have our flags isolated we can actually submit it for scoring. This is actually super easy, since Google Chrome has an option to export a web request (like submitting a flag for example) to `curl` a command for making web requests. [Example](./resources/copyAsCurl.png) (I copied a random request, as I don't have access to the submit server y'kno)

Now do the same thing with `os.popen` and you can simply read the output to see if it was successful or not

```
text = os.popen("curl 'https://www.google.com/search?q=reee&oq=reee' -H 'authority: www.google.com' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3' -H 'referer: https://www.google.com/' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: en-US,en;q=0.9' -H 'cookie:1P_JAR=2019-4-19-17' --compressed")
```

And text will have our output, in the case it returns json, you can match if a string exists in the output

```python
if 'success' in text:
	print(':)')
```



oorah cyber marine